<?php
class Person {

	public $firstName;
	public $middleName;
	public $lastName;
	

	//Constructor is used during the creation of an object.

	public function __construct($firstName, $middleName, $lastName){

		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;

	}

	public function printName(){

		return "Your full name is $this->firstName $this->lastName";
	}
}

class Developer extends Person{

	// Inheritance
	public function printName(){

		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}

}

class Engineer extends Person{

	// Inheritance
	public function printName(){

		return "Your are an egineer named $this->firstName $this->middleName $this->lastName";
	}

}



$person = new Person('Senku', 'Mid', 'Ishikigami');

$developer = new Developer('John', 'Flinch', 'Smith');

$engineer = new Engineer('Harold', 'Myers', 'Reese');