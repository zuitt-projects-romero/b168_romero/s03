<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03: Classes and Objects</title>
</head>
<body>
	<h1>Objects from Variables</h1>

	<p><?php echo $buildingObj->name; ?></p>
	<p><?php echo $buildingObj->address->country; ?></p>

	<h1>Objects and Classes</h1>

	<p><?php var_dump($building); ?></p>
	
	<!-- <p>< ?php echo $building->printName(); ?></p -->>

	<h1>Inheritance (Condominium Object)</h1>

	<p><?php echo $condominium->name; ?></p>

	<p><?php echo $condominium->printName(); ?></p>

	<h1>Polymorphism (Changing of printName Behavior)</h1>

	<p><?php echo $condominium->printEnzo(); ?></p>

</body>
</html>
